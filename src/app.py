from flask import Flask
from functional import *

app = Flask(__name__)

@app.route('/')
def example():
    number = request.args.get('number')
    option = request.args.get('option')
    return functional.solution(number, option)
    
            